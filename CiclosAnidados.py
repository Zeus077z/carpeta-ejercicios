#Esto centra lo que escribí
mensajeEntrada="programa de generación de tabla".capitalize()
print(mensajeEntrada.center(60,"="))

opcion=1
opcion='S'
#Para las opciones de salir n
while opcion!='n' and opcion!='N':
    #Ingresa los valores que tendrán las tablas
    tabInicio = int(input(chr(27)+"[1;103m"+"Ingrese la tabla inicial: "))
    tabFinal = int(input(chr(27)+"[1;103m"+"Ingrese la tabla final: "))

    while tabFinal<tabInicio:
        print("La tabla de incio debe ser menor")
        tabInicio = int(input("Ingrese la tabla inicial: "))
        tabFinal = int(input("Ingrese la tabla final: \n"))
    #Ingresa el rango de la tabla
    rangoIncio = int(input("Ingrese el rango inicial: "))
    rangoFinal = int(input("Ingrese el rango final: "))

    while rangoFinal<rangoIncio:
        print("El rango incial debe ser menor")
        rangoIncio = int(input("Ingrese el rango inicial: "))
        rangoFinal = int(input("Ingrese el rango final: "))

    while tabInicio<tabFinal:
        for i in range(tabInicio,tabFinal+1):
            #Para que continue y salte el numero 4
            if i==4:
                print("La tabla {0} no la imprimo porque no quiero".format(i))
                continue
                pass
            for j in range(rangoIncio,rangoFinal+1):
                #Para que salte cuando vaya a multiplicar por 5
                if j==5:
                    print("Mas de 5 no quiero")
                    break
                resultado=i*j
                print("Multiplicar %d * %d es igual a %d"%(i,j,resultado))

        tabInicio+=1
        opcion=(input("Desea ejecutar nuevamente el proceso? : \n"
                     "s. Continuar\n"
                     "n. Salir \n"))
        if (opcion == 'n') or (opcion=='N'):
            break
else:
    print("Gracias por jugar")